diagram <- "flowchart LR\n  A --> B"

test_that("mermaid_gen_link: conversion of mmd to pako works", {
  pako <- "https://mermaid.ink/img/pako:eNqrVkrOT0lVslJKy8kvT85ILCpR8AmKyVNQcFTQ1bVTcFLSUcpNLcpNzExRsqpWKslIzQUpTklNSyzNKVGqrQUAjIcUfg?type=png"
  # multiline string
  expect_equal(mermaid_gen_link(diagram), pako)
  # character vector
  diagram <- strsplit(diagram, "\n")[[1]]
  expect_equal(mermaid_gen_link(diagram), pako)
  # from file
  f <- tempfile()
  writeLines(diagram, f)
  expect_equal(mermaid_gen_link(f), pako)
  unlink(f)
})

test_that("mermaid returns a file", {
  f <- mermaid(diagram)
  file.dest <- basename(f)
  expect_true(file.exists(f))
  f <- mermaid(diagram, file.dest = "toto.png")
  expect_equal(f, file.path(tempdir(), "toto.png"))
  expect_true(file.exists(f))
  f <- mermaid(diagram, dir.dest = file.path(tempdir(), "try-me"))
  expect_equal(f, file.path(tempdir(), "try-me", file.dest))
  expect_true(file.exists(f))
  f <- mermaid(diagram, dir.dest = file.path(tempdir(), "try-me"), file.dest = "toto.png")
  expect_equal(f, file.path(tempdir(), "try-me", "toto.png"))
  expect_true(file.exists(f))
  unlink(f)
})
