#' Update mermaid.js in the DiagrammeR package
#'
#' @description
#' The mermaid js script integrated in DiagrammeR is quiet old. This function
#' updates the script directly in the DiagrammeR package.
#'
#' This update can have some issues for particular usages
#' (See https://github.com/rich-iannone/DiagrammeR/issues/475).
#'
#' @param url [character] generic url where to download the file "mermaid.min.js"
#' @param version [character] specific version to download (eg.: "9.3.0"), use
#' an empty string for the latest (default)
#'
#' @return Value returned by [download.file]
#' @export
#'
#' @examples
#' \dontrun{
#' updateMermaid()
#' }
updateMermaid <-
  function(url = "https://cdn.jsdelivr.net/npm/mermaid@version/dist/mermaid.min.js",
           version = "") {
    if (version != "") {
      stopifnot(grepl("^[[:digit:]]+\\.[[:digit:]]+\\.[[:digit:]]+$", version))
      version <- paste0("@", version)
    }
    url <- gsub("@version", version, url)
    try(download.file(
      url,
      system.file(
        "htmlwidgets/lib/mermaid/dist/mermaid.slim.min.js",
        package = "DiagrammeR"
      )
    ))
  }
