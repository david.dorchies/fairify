## Create a *fairify* project

`create_fairify` creates a R-package skeleton and open your *fairify*
project on Rstudio:

    # Create a project in a temporary folder for the example
    path <- file.path(tempdir(), "my_fairify_project")
    fairify::create_fairify(path)

    ## ✔ Creating 'C:/Users/DDORCH~1/AppData/Local/Temp/RtmpsJQiJu/my_fairify_project/'
    ## ✔ Setting active project to 'C:/Users/ddorchies/AppData/Local/Temp/RtmpsJQiJu/my_fairify_project'
    ## ✔ Creating 'R/'
    ## ✔ Writing 'DESCRIPTION'

    ## Package: my_fairify_project
    ## Title: What the Package Does (One Line, Title Case)
    ## Version: 0.0.0.9000
    ## Authors@R (parsed):
    ##     * First Last <first.last@example.com> [aut, cre] (YOUR-ORCID-ID)
    ## Description: What the package does (one paragraph).
    ## License: `use_mit_license()`, `use_gpl3_license()` or friends to pick a
    ##     license
    ## Remotes:
    ##     git::https://forgemia.inra.fr/umr-g-eau/fairify.git
    ## Encoding: UTF-8
    ## Roxygen: list(markdown = TRUE)
    ## RoxygenNote: 7.3.1

    ## ✔ Writing 'NAMESPACE'
    ## ✔ Setting active project to '<no active project>'
    ## ✔ Setting active project to 'C:/Users/ddorchies/AppData/Local/Temp/RtmpsJQiJu/my_fairify_project'
    ## ✔ Adding 'fairify' to Depends field in DESCRIPTION
    ## • Are you sure you want Depends? Imports is almost always the better choice.
    ## ✔ Setting active project to 'C:/Users/ddorchies/AppData/Local/Temp/RtmpsJQiJu/my_fairify_project'
    ## ✔ Adding '^_pkgdown\\.yml$', '^public$', '^pkgdown$' to '.Rbuildignore'
    ## ✔ Adding 'public' to '.gitignore'
    ## Fairify project created
    ## 
    ## Don't forget to edit the file `inst/config.yml` for configuring the data location for your project
    ## 
    ## ✔ Setting active project to 'C:/Users/ddorchies/AppData/Local/Temp/RtmpsJQiJu/my_fairify_project'

    list.files(path)

    ## [1] "_pkgdown.yml" "DESCRIPTION"  "inst"         "NAMESPACE"    "R"           
    ## [6] "reports"      "templates"

## Create reports with **bookdown**

By default, the *fairify* project contains a `reports` folder which will
contain bookdown reports.

Use the function `fairify::add_report` for creating a new report.

    # Create a report
    fairify::add_report("my_report", template = "umr_geau", path = path)

    ## [1] "C:\\Users\\DDORCH~1\\AppData\\Local\\Temp\\RtmpsJQiJu/my_fairify_project/reports/my_report"

    # Add a chapter
    writeLines(
      c(
        "# A new chapter",
        "",
        "With a new paragraph."
      ),
      file.path(path, "reports/my_report/01-new_chapter.Rmd")
    )

Read the [documentation of
bookdown](https://bookdown.org/yihui/bookdown/) for more details on how
to write great reports with references, figures, and formulas.

The template parameter allows to choose the LaTeX template used for
rendering the PDF report. The LaTeX template settings can be overloaded
by modifying the files “`in_header.tex`”, “`before_body.tex`”, and
“`after_body.tex`” located in the report folder ([See bookdown
documentation for
details](https://bookdown.org/yihui/bookdown/yaml-options.html)).

## Render reports

By default, the reports are rendered in the public folder of the
*fairify* project.

    # Render the report in HTML format
    fairify::render_reports(reports_dir = file.path(path, "reports"))

    ## folder list:

    ## my_report

    ## *******************************************************************

    ## ** RENDER my_report

    ## *******************************************************************

    ## output_format=bookdown::gitbook

    ## Rendering book in directory 'C:\Users\DDORCH~1\AppData\Local\Temp\RtmpsJQiJu/my_fairify_project/reports/my_report'

    ## processing file: report.Rmd

    ## output file: report.knit.md

    ## "C:/Program Files/RStudio/resources/app/bin/quarto/bin/tools/pandoc" +RTS -K512m -RTS report.knit.md --to html4 --from markdown+autolink_bare_uris+tex_math_single_backslash --output report.html --lua-filter "C:\Users\ddorchies\AppData\Local\Programs\R\R-4.3.1\library\bookdown\rmarkdown\lua\custom-environment.lua" --lua-filter "C:\Users\ddorchies\AppData\Local\Programs\R\R-4.3.1\library\rmarkdown\rmarkdown\lua\pagebreak.lua" --lua-filter "C:\Users\ddorchies\AppData\Local\Programs\R\R-4.3.1\library\rmarkdown\rmarkdown\lua\latex-div.lua" --lua-filter "C:\Users\ddorchies\AppData\Local\Programs\R\R-4.3.1\library\rmarkdown\rmarkdown\lua\anchor-sections.lua" --metadata-file "C:\Users\DDORCH~1\AppData\Local\Temp\RtmpsJQiJu\file5664ef05c75" --wrap preserve --standalone --section-divs --table-of-contents --toc-depth 3 --template "C:\Users\ddorchies\AppData\Local\Programs\R\R-4.3.1\library\bookdown\templates\gitbook.html" --highlight-style pygments --number-sections --css style.css --mathjax --include-in-header "C:\Users\DDORCH~1\AppData\Local\Temp\RtmpsJQiJu\rmarkdown-str56644c4a78c5.html" --citeproc

    ## 
    ## Output created: C:/Users/DDORCH~1/AppData/Local/Temp/RtmpsJQiJu/my_fairify_project/public/reports/my_report/index.html

    # Render the report in PDF format
    fairify::render_reports(reports_dir = file.path(path, "reports"),
                   output_format = "bookdown::pdf_book")

    ## folder list:

    ## my_report

    ## *******************************************************************

    ## ** RENDER my_report

    ## *******************************************************************

    ## output_format=bookdown::pdf_book

    ## Rendering book in directory 'C:\Users\DDORCH~1\AppData\Local\Temp\RtmpsJQiJu/my_fairify_project/reports/my_report'

    ## processing file: report.Rmd

    ## output file: report.knit.md

    ## "C:/Program Files/RStudio/resources/app/bin/quarto/bin/tools/pandoc" +RTS -K512m -RTS report.knit.md --to latex --from markdown+autolink_bare_uris+tex_math_single_backslash --output report.tex --lua-filter "C:\Users\ddorchies\AppData\Local\Programs\R\R-4.3.1\library\bookdown\rmarkdown\lua\custom-environment.lua" --lua-filter "C:\Users\ddorchies\AppData\Local\Programs\R\R-4.3.1\library\rmarkdown\rmarkdown\lua\pagebreak.lua" --lua-filter "C:\Users\ddorchies\AppData\Local\Programs\R\R-4.3.1\library\rmarkdown\rmarkdown\lua\latex-div.lua" --metadata-file "C:\Users\DDORCH~1\AppData\Local\Temp\RtmpsJQiJu\file5664239a1d39" --embed-resources --standalone --table-of-contents --toc-depth 2 --number-sections --highlight-style tango --pdf-engine pdflatex --biblatex --include-in-header in_header.tex --include-before-body before_body.tex --include-after-body after_body.tex --variable graphics --wrap preserve --variable tables=yes --standalone -Mhas-frontmatter=false

    ## 
    ## Output created: C:/Users/DDORCH~1/AppData/Local/Temp/RtmpsJQiJu/my_fairify_project/public/reports/my_report/report.pdf

## Use data located on the cloud (or not…)

### How it works?

The *fairify* project provides a function `getDataPath` which helps you
to use data located on the cloud in your R scripts.

List of supported cloud servers:

-   [Owncloud](https://owncloud.com/)
-   [Nextcloud](https://nextcloud.com/)

To use it, you first need to define your cloud configuration in the file
`inst/config.yml`:

    default:
      data:
        mode: remote
        local: "path_for_data_on_your_disk"
        remote: "https://public_link_on_your_cloud"
        write_results: false

The `local` path stands for storing your data on a local or network disk
directly readable and writable by your Operating System.

### Caching results

`write_results` logical parameter is an helper for caching data results
of long calculations. The basic strategy is to either calculate and save
a result or read this result depending of the value of this boolean:

    computeSomeThing <- function(cfg = loadConfig()) {
      if (cfg$data$write_results) {
        myBigResult <- 2 + 2
        saveRDS(myBigResult, getDataPath("myBigResult.RDS"))
      } else {
        myBigResult <- readRDS(getDataPath("myBigResult.RDS"))
      }
      return(myBigResult)
    }
